<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>College Funding Survey</title>
  </head>
  <body class="bg-light">

  <!-- Just an image -->
  <nav class="navbar navbar-light">
     <div class = "container">
     <a class="navbar-brand" href="/survey">
        <img src="assets/img/logo.jpg" width="100" height="100" alt="logo">
     </a> 
     </div>
  </nav>
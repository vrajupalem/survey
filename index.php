<?php
include_once 'connection.php';
 // Set error
 $error="";

if($_SERVER["REQUEST_METHOD"] == "POST"){
   
   // Checking if the form data is not empty, else we store data in respective variables
   if (empty($_POST["gpa"])){
       $error .= "<div class='alert alert-danger' role='alert'>GPA is required</div>";
   } else {
       $gpa = $_POST["gpa"];
   }
   if (empty($_POST["name"])){
    $error .= "<div class='alert alert-danger' role='alert'>Name is required</div>";
   } else {
       $name = $_POST["name"];
   }

   if (empty($_POST["email"])){
    $error .= "<div class='alert alert-danger' role='alert'>Email is required</div>";
   } else {
       $email = $_POST["email"];
   }

   if (empty($_POST["college"])){
    $error .= "<div class='alert alert-danger' role='alert'>College is required</div>";
   } else {
       $college = $_POST["college"];
   }

   if (empty($_POST["class_year"])){
    $error .= "<div class='alert alert-danger' role='alert'>Class Year is required</div>";
   } else {
       $class_year = $_POST["class_year"];
   }

   if (empty($_POST["high_school"])){
    $error .= "<div class='alert alert-danger' role='alert'>High School is required</div>";
   } else {
       $high_school = $_POST["high_school"];
   }

   if (empty($_POST["funding_source"])){
    $error .= "<div class='alert alert-danger' role='alert'>Funding Source is required</div>";
   } else {
       $funding_source = $_POST["funding_source"];
   }

   // Storing data in the database
   if (empty($error)) {
    $stmt = $db->prepare("INSERT INTO inputs (gpa, name, email, college, class_year, high_school, funding_source) values (:gpa, :name, :email, :college, :class_year, :high_school, :funding_source)");
    $stmt->bindparam(':gpa', $gpa);
    $stmt->bindparam(':name', $name);
    $stmt->bindparam(':email', $email);
    $stmt->bindparam(':college', $college);
    $stmt->bindparam(':class_year', $class_year);
    $stmt->bindparam(':high_school', $high_school);
    $stmt->bindparam(':funding_source', $funding_source);
 
    $stmt->execute();

    $error = '
        <div class="alert alert-success" role="alert">
            Success, Thank You for Submitting!
        </div>
    ';
   } else {
       // return: $error;
       //$error = '
       // <div class="alert alert-danger" role="alert">
       //     ERROR in Submitting!
      //  </div>
    //';
   }
   

}?>

<?php include 'templates/header.php'?>
<section id="surveyForm">
    <div class="container">
          <div class="row mx-0">
               <div class="col-12">
                   <?php echo $error; ?>
                   <div class="card tablem">
                       <div class="card-body">
                            <h5 class="card-title">College Funding Survey</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="row mx-0 px-0">
                                    <div class="form-group col-12 col-md-6">
                                        <label for="name">Full Name</label>
                                        <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp" placeholder="Enter Name">
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="email">Email Address</label>
                                        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter Email">
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="name">GPA</label>
                                        <input type="number" step=".01" class="form-control" id="gpa" name="gpa" aria-describedby="gpaHelp" placeholder="Enter GPA">
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="class_year">Class Year</label>
                                        <input type="number" min="1900" max="9999" class="form-control" id="class_year" name="class_year" aria-describedby="classYearHelp" placeholder="Enter Class Year">
                                    </div>

                                    <div class="form-group col-12">
                                        <label for="high_school">High School</label>
                                        <input type="text" class="form-control" id="high_school" name="high_school" aria-describedby="highSchoolHelp" placeholder="Enter High School">
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="college">College</label>
                                        <input type="text" class="form-control" id="college" name="college" aria-describedby="collegeHelp" placeholder="Enter College">
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="funding_source">Funding Source</label>
                                        <input type="text" class="form-control" id="funding_source" name="funding_source" aria-describedby="fundingSourceHelp" placeholder="Enter Funding Source">
                                    </div>

                                    <div class="form-group col-12">
                                         <button type="submit" class="btn btn-primary">Submit</button>
                                         <a href="/survey/admin">Admin</a>
                                    </div>
                            </form>
                        </div>
                    </div>
               </div>

          </div>
    </div>
</section>
<?php include 'templates/footer.php'?>

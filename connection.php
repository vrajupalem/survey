<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "survey";

try {
    $db = new PDO("mysql: host=$servername; dbname=$dbname", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Database Connected successfully";
    echo "<script>console.log('Database Connected successfully');</script>";
} 
catch (PDOException $e) {
     echo "Database Connection failed: " . $e->getMessage();
}
?>
<?php


// Display all of the inputs in a table
function inputTable() {
   include 'connection.php';
   $sql = "SELECT * FROM inputs ORDER BY date DESC";
   $results = $db->query($sql);

   if ($results->rowCount() > 0) {
       while($row = $results->fetch(PDO::FETCH_ASSOC)) {

        echo '<tr>
        <td>'.$row["id"].'</td>
        <td>'.$row["gpa"].'</td>
        <td>'.$row["name"].'</td>
        <td>'.$row["email"].'</td>
        <td>'.$row["funding_source"].'</td>
        <td>'.$row["class_year"].'</td>
        <td>'.$row["high_school"].'</td>
        <td>'.$row["funding_source"].'</td>
        <td>'.$row["date"].'</td>
        <td><a href="/survey/admin?item=delete&id='.$row["id"].'" onClick="return confirm(\'Are you sure you want to delete this record\')">Delete</a></td>
        </tr>';

       }
   }
}

// Display how many inputs were submitted
function inputTotal() {
   include 'connection.php';
   $sql = "SELECT * FROM inputs";
   $results = $db->query($sql);

   $inputNum = $results->rowCount();

   echo $inputNum;
}

// Dispaly average GPA of the inputs
function avgGpa() {
   include 'connection.php';
   $sql = "SELECT AVG(gpa) AS gpa FROM inputs";
   $results = $db->query($sql);
   $avgGpa = $results->fetch(PDO::FETCH_ASSOC);
   echo round($avgGpa['gpa'], 2);
}

// Delete Input from URL parameters
function deleteInputs() {
   include 'connection.php';

   if(empty($_GET['item']) || empty($_GET['id'])){
      return;
   } else {
      $item = $_GET['item'];
      $id = $_GET['id'];

      if($item != 'delete'){
         return ;
      }else{
         if(empty($id)){
             return ;
         }else{
             $sql = "DELETE FROM inputs WHERE id=:id";
             $stmt = $db->prepare($sql);
             $stmt->bindParam(':id', $id);
             $stmt->execute();

         }
     }
   }

}
deleteInputs();
?>
<?php include 'templates/header.php'?>
<section id="surveyForm">
    <div class="container">
          <div class="row mx-0">
               <div class="col-12 col-md-6">
                   <div class="card numc">
                        <h1><small>Inputs</small></h1>
                        <h1><?php inputTotal(); ?></h1>
                   </div>
               </div>
               <div class="col-12 col-md-6">
                   <div class="card numc">
                        <h1><small>AVG GPA</small></h1>
                        <h1><?php avgGpa(); ?></h1>
                   </div>
               </div>
               
               <div class="col-12">
                   <div class="card tablem">
                   <table class="table table-striped tablecard">
                        <thead>
                           <tr>
                              <th scope="col">#</th>
                              <th scope="col">GPA</th>
                              <th scope="col">Name</th>
                              <th scope="col">Email</th>
                              <th scope="col">College</th>
                              <th scope="col">Yr</th>
                              <th scope="col">HS</th>
                              <th scope="col">Source</th>
                              <th scope="col">Date</th>
                              <th scope="col">Manage</th>
                           </tr>
                        </thead>
                        <tbody>
                              <?php inputTable(); ?>
                        </tbody>
                    </table>
                   </div>
               </div>
            </div>
      </div>

</section>
<?php include 'templates/footer.php'?>